const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use("/" , express.static("../front-end"));
const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});
connection.connect(function (err) {
    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),varsta VARCHAR(3),cnp VARCHAR(20), sex VARCHAR(1), telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER)";
    connection.query(sql, function (err, result) {
        if (err) {
          throw err
        };
    });
});
app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let varsta = req.body.varsta;
    let cnp = req.body.cnp;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let error = [];
    let sex;
    function calculeaza_varsta(dob) { 
      let dif_milisec = Date.now() - dob.getTime();
      let anul1970_plusdifmilisec = new Date(dif_milisec); 
    
      return Math.abs(anul1970_plusdifmilisec.getFullYear() - 1970);
    }

    if (!nume||!prenume||!varsta||!cnp||!telefon||!email||!facebook||!tipAbonament||!nrCard||!cvv) {
        error.push("Unul sau mai multe campuri nu au fost introduse!");
        console.log("Unul sau mai multe campuri nu au fost introduse!");
    } else {
      if (nume.length < 3 || nume.length > 20){
        console.log("Nume invalid!");
        error.push("Nume invalid!");
      } else if (!nume.match("^[A-Za-z]+$")) {
        console.log("Numele trebuie sa contina doar litere!");
        error.push("Numele trebuie sa contina doar litere!");
      }
      if (prenume.length < 3 || prenume.length > 20) {
        console.log("Prenume invalid!");
        error.push("Prenume invalid!");
      } else if (!prenume.match("^[A-Za-z]+$")) {
        console.log("Prenumele trebuie sa contina doar litere!");
        error.push("Prenumele trebuie sa contina doar litere!");
      }
      if (varsta.length < 1 || varsta.length > 4){
          console.log("Varsta invalida!");
          error.push("Varsta invalida!");
      } else if (!varsta.match("^[0-9]+$")) {
        console.log("Varsta trebuie sa contina doar cifre!");
        error.push("Varsta trebuie sa contina doar cifre!");
      }
      if (cnp.length != 13){
        console.log("CNP invalid!");
        error.push("CNP invalid!");
    } else if (!cnp.match("^[0-9]+$")) {
      console.log("CNP-ul trebuie sa contina doar cifre!");
      error.push("CNP-ul trebuie sa contina doar cifre!");
    }
      if (parseInt(varsta) != calculeaza_varsta(new Date(parseInt(cnp[1]+cnp[2]), parseInt(cnp[3]+cnp[4]), parseInt(cnp[5]+cnp[6])))){
        console.log("Varsta nu este compatibila cu CNP-ul!");
        error.push("Varsta nu este compatibila cu CNP-ul!");
      }
      if (cnp[0] == 1 || cnp[0] == 3 || cnp[0] == 5) {
        sex="M";
      } else if (cnp[0] == 2 || cnp[0] == 4 || cnp[0] == 6) {
        sex="F";
      }
      if (telefon.length != 10) {
        console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
        error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
      } else if (!telefon.match("^[0-9]+$")) {
        console.log("Numarul de telefon trebuie sa contina doar cifre!");
        error.push("Numarul de telefon trebuie sa contina doar cifre!");
      }
      if (email.includes("@gmail.com") && email.includes("@yahoo.com")) {
        console.log("Email invalid!");
        error.push("Email invalid!");
      }
      if (!facebook.includes("https://www.facebook.com/")) {
          console.log("Link-ul catre pagina de facebook este invalid!");
          error.push("Link-ul catre pagina de facebook este invalid!");
    }
      if (nrCard.length != 16) {
        console.log("Numarul de pe card trebuie sa fie de 16 cifre!");
        error.push("Numarul de pe card trebuie sa fie de 16 cifre!");
      } else if (!nrCard.match("^[0-9]+$")) {
        console.log("Numarul de pe card trebuie sa contina doar cifre!");
        error.push("Numarul de pe card trebuie sa contina doar cifre!");
      }
      if (cvv.length != 3) {
        console.log("CVV-ul trebuie sa fie de 3 cifre!");
        error.push("CVV-ul trebuie sa fie de 3 cifre!");
      } else if (!cvv.match("^[0-9]+$")) {
        console.log("CVV-ul trebuie sa contina doar cifre!");
        error.push("CVV-ul trebuie sa contina doar cifre!");
      }
    }
    if (error.length === 0) {
      connection.query("SELECT email FROM abonament", function(err, rows){
        if (err) {
          throw err;
        }
        else {
          rows = JSON.stringify(rows);
          if (rows.includes(email)){
            error.push("Email-ul se afla deja in baza de date!");
            res.status(500).send(error);
            console.log("Eroare la inserarea in baza de date!");
          } else if (!rows.includes(email)) {
            const sql =
            "INSERT INTO abonament (nume,prenume,varsta,cnp,sex,telefon,email,facebook,tipAbonament,nrCard,cvv) VALUES('" +nume +"','" +prenume +"','" +varsta +"','" +cnp +"','" +sex +"','" +telefon +"','" +email+ "','" +facebook +"','" +tipAbonament +"','"+nrCard +"','" +cvv+"')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
            }
        };
      });
        
    } else {
        res.status(500).send(error);
        console.log("Eroare la inserarea in baza de date!");
    }
});